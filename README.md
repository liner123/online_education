# online_education

#### 介绍

online_education 是基于Spring boot + Vue 的前后端分离的网上教育网站，可以再上面上架视频和观看视频。

主要技术 
Spring boot 2.2.1 RELEASE
Spring Cloud Hoxton.RELEASE
aliyun oss
mysql
redis
Vue
Spring Security

#### 软件架构
软件架构说明

api-gateway 网关服务
common 通用类模块
service 服务模块
service-acl 登录、权限管理
service-cms 轮播图
service-edu 主要业务模块
service-msm 短信模块
service-order 订单模块
service-oss oss服务模块
service-statistics 统计模块
service-ucenter 前台页面模块
service-vod 视频上传模块

#### 安装教程

1.  git clone https://gitee.com/liner123/online_education.git
2.  导入数据库文件 sql
3.  使用eclipse 或者 idea 打开
4.  修改 mysql 、 nacos 、redis 地址为自己的地址
5.  如要使用oss等服务 ,请修改 密码等为自己的
6.  前端项目地址  https://gitee.com/liner123/online_education_vue.git
#### 使用说明

无 (高兴就好)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
