package com.ithl.edumsm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author hl
 * @Data 2020/4/22
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@ComponentScan({"com.ithl"})
@EnableDiscoveryClient
public class MsmApplicationMain8005 {
    public static void main(String[] args) {
        SpringApplication.run(MsmApplicationMain8005.class, args);
    }
}
