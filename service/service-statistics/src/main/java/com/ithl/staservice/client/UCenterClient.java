package com.ithl.staservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author hl
 * @Data 2020/4/29
 */
@Component
@FeignClient("service-ucenter")
public interface UCenterClient {
    // 查询当天的注册人数
    @GetMapping("/ucenter/member/getRegisterCount")
    public int getRegisterCount(@RequestParam("day") String day);
}
