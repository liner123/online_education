package com.ithl.educenter.service;

import com.ithl.educenter.entity.UcenterMember;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ithl.educenter.entity.vo.RegisterVo;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author hl
 * @since 2020-04-23
 */
public interface UcenterMemberService extends IService<UcenterMember> {

    String login(UcenterMember ucenterMember);

    void register(RegisterVo registerVo);

    UcenterMember getByOpenId(String openId);

    Integer getCountByDate(String day);
}
