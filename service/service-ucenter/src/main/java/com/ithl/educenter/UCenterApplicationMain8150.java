package com.ithl.educenter;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author hl
 * @Data 2020/4/23
 */
@SpringBootApplication
@MapperScan("com.ithl.educenter.mapper")
@ComponentScan(basePackages = {"com.ithl"})
@EnableDiscoveryClient
public class UCenterApplicationMain8150 {

    public static void main(String[] args) {
        SpringApplication.run(UCenterApplicationMain8150.class, args);
    }
}
