package com.ithl.educenter.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author hl
 * @Data 2020/4/24
 */
@Component
public class ConstantVxUtils implements InitializingBean {


    @Value("${wenxin.open.appid}")
    private String appId;
    @Value("${wenxin.open.appsecret}")
    private String appSecret;
    @Value("${wenxin.open.redirecturl}")
    private String redirectUrl;

    public static String WX_OPEN_APP_ID;
    public static String WX_OPEN_APP_SECRET;
    public static String WX_OPEN_REDIRECT_URL;

    @Override
    public void afterPropertiesSet() throws Exception {
        WX_OPEN_APP_ID = appId;
        WX_OPEN_APP_SECRET = appSecret;
        WX_OPEN_REDIRECT_URL = redirectUrl;
    }
}
