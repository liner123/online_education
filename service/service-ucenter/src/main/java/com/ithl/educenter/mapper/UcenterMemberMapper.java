package com.ithl.educenter.mapper;

import com.ithl.educenter.entity.UcenterMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author hl
 * @since 2020-04-23
 */
public interface UcenterMemberMapper extends BaseMapper<UcenterMember> {

    Integer getCountByDate(String day);
}
