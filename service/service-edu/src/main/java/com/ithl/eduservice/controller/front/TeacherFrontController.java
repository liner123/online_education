package com.ithl.eduservice.controller.front;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ithl.commonutils.R;
import com.ithl.eduservice.entity.EduCourse;
import com.ithl.eduservice.entity.EduTeacher;
import com.ithl.eduservice.service.EduCourseService;
import com.ithl.eduservice.service.EduTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author hl
 * @Data 2020/4/24
 */
@RestController
//@CrossOrigin
@RequestMapping("/eduservice/teacherfront")
public class TeacherFrontController {
    @Autowired
    private EduTeacherService teacherService;
    @Autowired
    private EduCourseService courseService;

    /**
     * 查询前八名讲师
     */
    @PostMapping("/getTeacherList/{page}/{limit}")
    public R getTeacherList(@PathVariable("page") long page, @PathVariable("limit") long limit) {
        Page<EduTeacher> teacherPage = new Page<>(page, limit);
        Map<String, Object> map = teacherService.getTeacherFront(teacherPage);
        // 返回分页数据
        return R.ok().data(map);
    }

    /**
     * 讲师详情
     */
    @GetMapping("/getTeacherInfo/{teacherId}")
    public R getTeacherInfo(@PathVariable String teacherId) {
        // 查询讲师的信息
        EduTeacher eduTeacher = teacherService.getById(teacherId);
        // 查询讲师的课程
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        wrapper.eq("teacher_id", teacherId);
        List<EduCourse> eduList = courseService.list(wrapper);

        return R.ok().data("eduTeacher", eduTeacher).data("eduList", eduList);
    }
}
