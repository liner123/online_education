package com.ithl.eduorder.client;

import com.ithl.commonutils.orderVo.UcenterMemberOrder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author hl
 * @Data 2020/4/27
 */
@Component
@FeignClient("service-ucenter")
public interface UCenterClient {

    // 根据用户id 查询用户信息
    @GetMapping("/ucenter/member/getUserInfoOrder")
    public UcenterMemberOrder getUserInfoOrder(@RequestParam("memberId") String memberId);
}
