package com.athl.aclservice.mapper;


import com.athl.aclservice.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * Mapper 接口
 *
 * @author hl
 */
public interface RoleMapper extends BaseMapper<Role> {

}
