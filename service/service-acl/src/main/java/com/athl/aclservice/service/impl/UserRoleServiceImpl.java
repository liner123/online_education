package com.athl.aclservice.service.impl;


import com.athl.aclservice.entity.UserRole;
import com.athl.aclservice.mapper.UserRoleMapper;
import com.athl.aclservice.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 服务实现类
 *
 * @author hl
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
