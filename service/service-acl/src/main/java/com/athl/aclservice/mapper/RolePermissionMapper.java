package com.athl.aclservice.mapper;

import com.athl.aclservice.entity.RolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 角色权限 Mapper 接口
 *
 * @author hl
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
