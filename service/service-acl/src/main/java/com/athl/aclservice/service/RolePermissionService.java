package com.athl.aclservice.service;


import com.athl.aclservice.entity.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 角色权限 服务类
 *
 * @author hl
 */
public interface RolePermissionService extends IService<RolePermission> {

}
